module "gcp-firewall" {
  source = "../."

  project=var.project
  network=var.network

  allow_rules = [{
    name="test-rule"
    direction="INGRESS"
    source_tags=["source"]
    target_tags=["target"]
    ports=["80"]
    protocol="TCP"
  }]
}