resource "google_compute_firewall" "allow_rules" {
  for_each = { for v in var.allow_rules : v.name => v }
  project  = var.project
  network  = var.network

  name          = format("%s-%s", var.prefix_allow, each.value["name"])
  direction     = lookup(each.value, "direction", "INGRESS")
  source_ranges = each.value["source_ranges"]
  source_tags   = each.value["source_tags"]
  target_tags   = each.value["target_tags"]

  allow {
    protocol = lookup(each.value, "protocol", "TCP")
    ports    = lookup(each.value, "ports", [])
  }
}

resource "google_compute_firewall" "deny_rules" {
  for_each = { for i, v in var.deny_rules : i => v }
  project  = var.project
  network  = var.network

  name          = format("%s-%s", var.prefix_allow, each.value["name"])
  direction     = lookup(each.value, "direction", "INGRESS")
  source_ranges = each.value["source_ranges"]
  source_tags   = each.value["source_tags"]
  target_tags   = each.value["target_tags"]

  deny {
    protocol = lookup(each.value, "protocol", "TCP")
    ports    = lookup(each.value, "ports", [])
  }
}
