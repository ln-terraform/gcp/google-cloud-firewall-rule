variable "project" {
  type = string
}

variable "network" {
  type = string
}

variable "prefix_allow" {
  type    = string
  default = "terraform-allow"
}

variable "prefix_deny" {
  type    = string
  default = "terraform-deny"
}

variable "allow_rules" {
  type = list(object({
    name          = string
    source_ranges = list(string)
    source_tags   = list(string)
    target_tags   = list(string)
    protocol      = string
    ports         = list(string)
  }))
  default = []
}

variable "deny_rules" {
  type = list(object({
    name          = string
    source_ranges = list(string)
    source_tags   = list(string)
    target_tags   = list(string)
    protocol      = string
    ports         = list(string)
  }))
  default = []
}